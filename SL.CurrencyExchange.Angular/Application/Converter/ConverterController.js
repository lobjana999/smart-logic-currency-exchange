﻿
function ConverterController($scope, currenciesService, converter) {

    $scope.currencies = [];

    $scope.initializeCurrencies = function () {

        var bind = function (currencies) {
            $scope.currencies = currencies;
        }

        currenciesService.getAll().success(bind);
    }

    $scope.convert = function () {

        if (!$scope.validateConversion()) {
            return;
        }

        var bind = function (result) { $scope.conversionResult = result; }
        converter.convert
        (
            $scope.currencyFrom,
            $scope.currencyTo,
            $scope.amount
        ).success(bind);
    }

    $scope.validateConversion = function () {

        if ($scope.currencyFrom == null) {
            alert("currency from is  empty");
            return false;
        }

        if ($scope.currencyTo == null) {
            alert("currency to is  empty");
            return false;
        }

        if ($scope.amount == null) {
            alert("amount is  empty");
            return false;
        }

        return true;
    }

    $scope.initializeCurrencies();
}

currencyExchangeApp.controller("ConverterController", ["$scope", "CurrenciesService", "ConversionService", ConverterController]);