﻿
function ReportController($scope, $filter, currenciesService, changeHistoryService) {

    $scope.currencies = [];
    $scope.selectedCurrencies = [];
    $scope.series = [];

    $scope.initialize = function () {
        $scope.initializeCurrencies();
        $scope.initializeCurrenciStabilities();
    }

    $scope.initializeCurrencies = function () {

        var bind = function (currencies) {
            $scope.currencies = currencies;
        }

        currenciesService.getAll().success(bind);
    }

    $scope.initializeCurrenciStabilities = function () {

        var bindMostStable = function (currencies) {
            $scope.mostStableCurrencies = currencies;
        };

        currenciesService.getMostStable(3).success(bindMostStable);

        var bindMostUnstable = function (currencies) {
            $scope.mostUnstableCurrencies = currencies;
        };

        currenciesService.getMostUnstable(3).success(bindMostUnstable);
    }

    $scope.addCurrency = function () {

        $scope.selectedCurrencies.push($scope.selectedCurrency);

        var bind = function (history) {
            $scope.changeHistory = history;
            $scope.refreshChart();
        }

        changeHistoryService.getChangeHistory($scope.selectedCurrencies).success(bind);
    }

    $scope.refreshChart = function () {

        var pallette = new Rickshaw.Color.Palette();

        angular.forEach($scope.changeHistory, function (changes, currency) {

            var points = [];

            for (var i = 0; i < changes.length; i++) {
                points[i] = { x: new Date(changes[i].date).getTime(), y: changes[i].rate };
            }

            var param = { color: pallette.color(), data: points, name: currency }

            $scope.series.push(param);
        });
    }

    $scope.chartSettings = {
        options: {
            renderer: "line",
            height: 500
        },
        features: {
            hover: {
                xFormatter: function (x) {
                    var changeDate = new Date(x);
                    return "Change Date=" + $filter("date")(changeDate, "dd/MM/yyyy");
                },
                yFormatter: function (y) {
                    return "Rate=" + y;
                }
            }
        }
    }

    $scope.initialize();
}


currencyExchangeApp.controller("ReportController", ["$scope", "$filter", "CurrenciesService", "ChangeHistoryService", ReportController]);