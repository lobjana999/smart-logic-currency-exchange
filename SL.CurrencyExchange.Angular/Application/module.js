﻿
var currencyExchangeApp = angular.module("CurrencyExchange", ["ngRoute", "CurrencyExchangeServices","angular-rickshaw"]);

function RouteProvider($routeProvider) {
    $routeProvider.
        when
        (
            "/converter",
            { templateUrl: "Application/Converter/converter.html", controller: "ConverterController" }
        )
        .when
        (
            "/reports",
            { templateUrl: "Application/Report/report.html", controller: "ReportController" }
        )
        .otherwise
        (
            { redirectTo: "/converter" }
        );
}

currencyExchangeApp.config(["$routeProvider", RouteProvider]);