﻿

function ConversionService(httpService) {

    this.resource = "api/convertedCurrencies/";

    this.convert = function (from, to, amount) {

        var params = { from: from, to: to, amount: amount };
        return httpService.getWithParams(this.resource, params);
    }

}


currencyExchangeServices.service("ConversionService", ["HttpService", ConversionService]);