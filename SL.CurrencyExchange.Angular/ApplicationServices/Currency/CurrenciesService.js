﻿

function CurrenciesService(httpService) {

    this.resource = "api/currencies";

    this.getAll = function () {
        return httpService.get(this.resource);
    }

    this.getMostStable = function (count) {

        var uri = this.resource + "/stable";

        return httpService.getWithParams(uri, { count: count });
    }

    this.getMostUnstable = function (count) {

        var uri = this.resource + "/unstable";

        return httpService.getWithParams(uri, { count: count });
    }
}

currencyExchangeServices.service("CurrenciesService", ["HttpService", CurrenciesService]);