﻿

function HttpService($http) {

    this.resource = "http://localhost:5823/";

    this.get = function (uri) {
        return $http.get(this.resource + uri);
    }

    this.getWithParams = function (uri, params) {
        return $http({
            url: this.resource + uri,
            method: "GET",
            params: params
        });
    }

    this.post = function (uri, data) {
        return $http.post(this.resource + uri, data);
    }
}

currencyExchangeServices.service("HttpService", ["$http", HttpService]);
