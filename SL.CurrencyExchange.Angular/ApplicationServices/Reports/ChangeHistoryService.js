﻿

function ChangeHistoryService(httpService) {

    this.resource = "api/currencyChangeHistory";

    this.getChangeHistory = function (currencies) {

        return httpService.getWithParams(this.resource, { currencies: currencies });
    }
}

currencyExchangeServices.service("ChangeHistoryService", ["HttpService", ChangeHistoryService]);