﻿using System;

namespace SL.CurrencyExchange.Domain.BusinessObjects
{
    public class CurrencyRate
    {
        public string Currency { get; set; }

        public decimal Rate { get; set; }

        public DateTime Date { get; set; }

        public CurrencyRate(decimal rate)
        {
            Rate = rate;
        }

        public CurrencyRate(string currency, decimal rate)
            : this(rate)
        {
            Currency = currency;
        }

        public CurrencyRate(string currency, decimal rate, DateTime date)
            : this(currency, rate)
        {
            Date = date;
        }
    }
}