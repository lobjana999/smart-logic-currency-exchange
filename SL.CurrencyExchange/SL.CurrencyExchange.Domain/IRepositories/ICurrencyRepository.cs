﻿using System;
using System.Collections.Generic;
using SL.CurrencyExchange.Domain.BusinessObjects;

namespace SL.CurrencyExchange.Domain.IRepositories
{
    public interface ICurrencyRepository
    {
        IEnumerable<CurrencyRate> GetCurrentRates();

        CurrencyRate GetCurrentRate(string code);

        Dictionary<DateTime, List<CurrencyRate>> GetRates();

        IEnumerable<CurrencyRate> GetFlattenRates();
        IList<string> GetAll();
    }
}