﻿using System;
using SL.CurrencyExchange.Domain.IRepositories;

namespace SL.CurrencyExchange.Domain.Services
{
    public class CurrencyConverter
    {
        private readonly ICurrencyRepository _repository;

        public CurrencyConverter(ICurrencyRepository repository)
        {
            _repository = repository;
        }

        public decimal Convert(decimal amount, string from, string to)
        {
            var currencyFrom = _repository.GetCurrentRate(from);
            if (currencyFrom == null)
            {
                throw new ArgumentException("currencyFrom not valid");
            }

            var currencyTo = _repository.GetCurrentRate(to);
            if (currencyTo == null)
            {
                throw new ArgumentException("currencyFrom not valid");
            }

            return amount * currencyTo.Rate / currencyFrom.Rate;
        }
    }
}
