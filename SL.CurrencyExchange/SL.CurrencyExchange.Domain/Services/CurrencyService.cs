﻿using System.Collections.Generic;
using SL.CurrencyExchange.Domain.IRepositories;

namespace SL.CurrencyExchange.Domain.Services
{
    public class CurrencyService
    {
        private readonly ICurrencyRepository _repository;

        public CurrencyService(ICurrencyRepository repository)
        {
            _repository = repository;
        }

        public IList<string> GetAll()
        {
            return _repository.GetAll();
        } 
    }
}
