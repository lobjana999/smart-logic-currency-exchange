﻿using System.Collections.Generic;
using System.Linq;
using SL.CurrencyExchange.Domain.BusinessObjects;
using SL.CurrencyExchange.Domain.IRepositories;

namespace SL.CurrencyExchange.Domain.Services
{
    public class CurrencyTracker
    {
        private readonly ICurrencyRepository _repository;
        private readonly IStabilityPercentageCalculator _stabilityCalculator;

        public CurrencyTracker(ICurrencyRepository repository, IStabilityPercentageCalculator stabilityCalculator)
        {
            _repository = repository;
            _stabilityCalculator = stabilityCalculator;
        }

        public Dictionary<string, List<CurrencyRate>> GetChangeHistory(params string[] currencies)
        {
            var rates = _repository.GetFlattenRates();
            if (currencies.Any())
            {
                rates = rates.Where(rate => currencies.Contains(rate.Currency));
            }

            var changesLookup = rates.ToList().ToLookup(key => key.Currency);
            return changesLookup.ToDictionary(key => key.Key, value => value.ToList());
        }

        public IList<KeyValuePair<string, decimal>> GetMostStableCurrencies(int defaultTake)
        {
            var percentages = _stabilityCalculator.Calculate(GetChangeHistory());

            return percentages.OrderBy(e => e.Value).Take(defaultTake).ToList();
        }

        public IList<KeyValuePair<string, decimal>> GetMostUnstableCurrencies(int defaultTake)
        {
            var percentages = _stabilityCalculator.Calculate(GetChangeHistory());

            return percentages.OrderByDescending(e => e.Value).Take(defaultTake).ToList();
        }
    }
}