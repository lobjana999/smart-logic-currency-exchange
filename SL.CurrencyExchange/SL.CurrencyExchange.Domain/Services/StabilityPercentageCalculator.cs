﻿using System;
using System.Collections.Generic;
using System.Linq;
using SL.CurrencyExchange.Domain.BusinessObjects;

namespace SL.CurrencyExchange.Domain.Services
{
    public interface IStabilityPercentageCalculator
    {
        IList<KeyValuePair<string, decimal>> Calculate(Dictionary<string, List<CurrencyRate>> history);
    }

    public class StabilityPercentageCalculator : IStabilityPercentageCalculator
    {
        public IList<KeyValuePair<string, decimal>> Calculate(Dictionary<string, List<CurrencyRate>> changesHistory)
        {
            var percentages = new List<KeyValuePair<string, decimal>>(changesHistory.Count);

            foreach (var pair in changesHistory)
            {
                var currencyChangePercentage = 0m;
                for (var i = 1; i < pair.Value.Count; i++)
                {
                    var current = pair.Value[i].Rate;
                    var previous = pair.Value[i - 1].Rate;

                    currencyChangePercentage += Math.Abs((current/previous - 1)*100);
                }

                percentages.Add(new KeyValuePair<string, decimal>(pair.Key, currencyChangePercentage));
            }

            return percentages.OrderByDescending(c => c.Value).ToList();
        }
    }
}
