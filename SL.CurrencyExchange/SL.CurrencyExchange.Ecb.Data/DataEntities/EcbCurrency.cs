using System;

namespace SL.CurrencyExchange.Ecb.Data.DataEntities
{
    public class EcbCurrency
    {
        public DateTime Date { get; set; }

        public string Currency { get; set; }

        public decimal Rate { get; set; }
    }
}