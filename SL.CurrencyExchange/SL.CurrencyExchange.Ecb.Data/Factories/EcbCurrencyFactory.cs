using System;
using System.Globalization;
using System.Xml.Linq;
using SL.CurrencyExchange.Ecb.Data.DataEntities;

namespace SL.CurrencyExchange.Ecb.Data.Factories
{
    public static class EcbCurrencyFactory
    {
        public static EcbCurrency CreateInstance(string date, string currency, string rate)
        {
            var ecbCurrency = new EcbCurrency
            {
                Date = DateTime.ParseExact(date, "yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo),
                Currency = currency,
                Rate = decimal.Parse(rate)
            };

            return ecbCurrency;
        }


        public static EcbCurrency CreateInstance(XElement cube, XElement currency)
        {
            return CreateInstance
                (
                    cube.Attribute(XName.Get("time")).Value,
                    currency.Attribute(XName.Get("currency")).Value,
                    currency.Attribute(XName.Get("rate")).Value
                );
        }
    }
}