﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;
using SL.CurrencyExchange.Domain.BusinessObjects;
using SL.CurrencyExchange.Domain.IRepositories;
using SL.CurrencyExchange.Ecb.Data.DataEntities;
using SL.CurrencyExchange.Ecb.Data.Factories;

namespace SL.CurrencyExchange.Ecb.Data.Repositories
{
    public class EcbCurrencyRepository : ICurrencyRepository
    {
        private readonly string _currencyXmlUri;
        private IList<EcbCurrency> _ecbCurrenciesDataSource;

        public EcbCurrencyRepository(string currencyXmlUri)
        {
            _currencyXmlUri = currencyXmlUri;
        }

        public EcbCurrencyRepository()
        {
            _currencyXmlUri = ConfigurationManager.AppSettings["EcbCurrencyXmlUri"];
            if (string.IsNullOrWhiteSpace(_currencyXmlUri))
            {
                throw new ConfigurationErrorsException("EcbCurrencyXmlUri in AppSettings is empty");
            }
        }

        public IEnumerable<CurrencyRate> GetCurrentRates()
        {
            var ratesDictionary = GetRates();
            var mostFreshCurrenciesDate = ratesDictionary.Keys.Max();

            return ratesDictionary[mostFreshCurrenciesDate];
        }

        public CurrencyRate GetCurrentRate(string code)
        {
            var currentRate = GetCurrentRates().FirstOrDefault(r => r.Currency.Equals(code, StringComparison.InvariantCultureIgnoreCase));

            return currentRate;
        }

        public Dictionary<DateTime, List<CurrencyRate>> GetRates()
        {
            var dataSource = GetFlattenRates();

            var ratesDictionary = dataSource.ToLookup(s => s.Date).ToDictionary(s => s.Key, s => s.ToList());

            return ratesDictionary;
        }

        public IEnumerable<CurrencyRate> GetFlattenRates()
        {
            var flattenRates = from rate in LoadCurrencies()
                               select new CurrencyRate(rate.Currency, rate.Rate, rate.Date);

            return flattenRates.ToList();
        }

        public IList<string> GetAll()
        {
            var currencies = LoadCurrencies();

            return currencies.Select(c => c.Currency).Distinct().ToList();
        }

        private IEnumerable<EcbCurrency> LoadCurrencies()
        {
            if (_ecbCurrenciesDataSource != null) return _ecbCurrenciesDataSource;

            var rootCubeElements = XElement.Load(_currencyXmlUri).Elements().Where(e => e.Name.LocalName.Equals("cube", StringComparison.InvariantCultureIgnoreCase));
            _ecbCurrenciesDataSource = (from rootCube in rootCubeElements
                                        from childCube in rootCube.Elements()
                                        from currency in childCube.Elements()
                                        select EcbCurrencyFactory.CreateInstance(childCube, currency)).ToList();

            return _ecbCurrenciesDataSource;
        }
    }
}
