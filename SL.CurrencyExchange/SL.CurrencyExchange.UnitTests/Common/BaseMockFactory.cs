using Moq;

namespace SL.CurrencyExchange.UnitTests.Common
{
    public abstract class BaseMockFactory<TSource> where TSource : class
    {
        protected Mock<TSource> Mock;

        protected BaseMockFactory()
        {
            Mock = new Mock<TSource>();
        }

        public abstract Mock<TSource> CreateInstance();
    }
}