﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using SL.CurrencyExchange.Domain.BusinessObjects;
using SL.CurrencyExchange.Domain.IRepositories;

namespace SL.CurrencyExchange.UnitTests.Common
{
    public class CurrencyRepositoryMockFactory : BaseMockFactory<ICurrencyRepository>
    {
        public override Mock<ICurrencyRepository> CreateInstance()
        {
            SetupGetCurrentRates();
            SetupGetCurrency();
            SetupGetRates();
            SetupGetFlattenRates();

            return Mock;
        }

        private void SetupGetCurrentRates()
        {
            Mock.Setup(m => m.GetCurrentRates()).Returns(() => new List<CurrencyRate>
            {
                new CurrencyRate("USD", 1.123m),
                new CurrencyRate("GEL", 2.256m)
            });
        }

        private void SetupGetCurrency()
        {
            Mock.Setup(m => m.GetCurrentRate(It.IsAny<string>())).Returns((string currency) =>
            {
                var rates = Mock.Object.GetCurrentRates();
                return rates.FirstOrDefault(c => c.Currency.Equals(currency, StringComparison.InvariantCultureIgnoreCase));
            });
        }

        private void SetupGetRates()
        {
            var ratesDictionary = new Dictionary<DateTime, List<CurrencyRate>>
            {
                {
                    DateTime.Now.Date,
                    new List<CurrencyRate>
                    {
                        new CurrencyRate("USD", 1.555m),
                        new CurrencyRate("GEL", 2.999m),
                        new CurrencyRate("PLN", 4.555m),
                        new CurrencyRate("RUB", 88.56m)
                    }
                },
                {
                    DateTime.Now.Date.AddDays(-1),
                    new List<CurrencyRate>
                    {
                        new CurrencyRate("USD", 1.777m), //changed: 12.4929%
                        new CurrencyRate("GEL", 2.555m), //changed: 14.8049%
                        new CurrencyRate("PLN", 6.111m), //changed: 25.4622%
                        new CurrencyRate("RUB", 130.56m) //changed: 32.1691%
                    }
                },
                {
                    DateTime.Now.Date.AddDays(-2),
                    new List<CurrencyRate>
                    {
                        new CurrencyRate("USD", 1.333m), //changed: 24.9859%
                        new CurrencyRate("GEL", 3.222m), //changed: 20.7014%
                        new CurrencyRate("PLN", 7.4352m), //changed: 17.8098%
                        new CurrencyRate("RUB", 125.55m) //changed: 3.8373%
                    }
                }
            };

            //USD: Total Change = 37.4788%
            //GEL: Total Change = 35.5063%
            //PLN: Total Change = 43.272%
            //RUB: Total Change = 36.0064%

            Mock.Setup(m => m.GetRates()).Returns(() => ratesDictionary);
        }

        private void SetupGetFlattenRates()
        {
            var ratesDictionary = new List<CurrencyRate>
            {
                new CurrencyRate("USD", 1.555m),
                new CurrencyRate("GEL", 2.999m),
                new CurrencyRate("PLN", 4.555m),
                new CurrencyRate("RUB", 88.56m),
                new CurrencyRate("USD", 1.777m),
                new CurrencyRate("GEL", 2.555m),
                new CurrencyRate("PLN", 6.111m),
                new CurrencyRate("RUB", 130.56m),
                new CurrencyRate("USD", 1.333m),
                new CurrencyRate("GEL", 3.222m),
                new CurrencyRate("PLN", 7.4352m),
                new CurrencyRate("RUB", 125.55m)
            };

            Mock.Setup(m => m.GetFlattenRates()).Returns(() => ratesDictionary);
        }
    }
}
