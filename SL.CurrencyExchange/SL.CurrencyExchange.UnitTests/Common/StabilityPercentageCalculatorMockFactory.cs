using System.Collections.Generic;
using Moq;
using SL.CurrencyExchange.Domain.BusinessObjects;
using SL.CurrencyExchange.Domain.Services;

namespace SL.CurrencyExchange.UnitTests.Common
{
    public class StabilityPercentageCalculatorMockFactory : BaseMockFactory<IStabilityPercentageCalculator>
    {
        public override Mock<IStabilityPercentageCalculator> CreateInstance()
        {
            SetupPecentages();

            return Mock;
        }

        private void SetupPecentages()
        {
            var percentages = new List<KeyValuePair<string, decimal>>
            {
                new KeyValuePair<string, decimal>("USD", 10),
                new KeyValuePair<string, decimal>("PLN", 20),
                new KeyValuePair<string, decimal>("GEL", 30),
                new KeyValuePair<string, decimal>("RUB", 40)
            };

            Mock.Setup(m => m.Calculate(It.IsAny<Dictionary<string, List<CurrencyRate>>>())).Returns(() => percentages);

        }
    }
}