﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SL.CurrencyExchange.Domain.IRepositories;
using SL.CurrencyExchange.Domain.Services;
using SL.CurrencyExchange.UnitTests.Common;

namespace SL.CurrencyExchange.UnitTests.Currency
{
    [TestClass]
    public class CurrencyConverterTests
    {
        private Mock<ICurrencyRepository> _repositoryMock;
        private CurrencyRepositoryMockFactory _mockFactory;

        [TestInitialize]
        public void Initialize()
        {
            _mockFactory = new CurrencyRepositoryMockFactory();
            _repositoryMock = _mockFactory.CreateInstance();
        }

        [TestMethod]
        public void Currency_Should_Converted_Correctly()
        {
            //arrange
            var converter = new CurrencyConverter(_repositoryMock.Object);

            //act
            var usdToGel = converter.Convert(1, "USD", "GEL");

            //assert
            const decimal expected = 2.008904719501336m;
            Assert.AreEqual(decimal.Round(usdToGel, 4), decimal.Round(expected, 4), "USD to GEL converted incorrectly");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Exception not thrown when currencyFrom is invalid")]
        public void Should_Rejected_When_CurrencyFrom_Is_Invalid()
        {
            //arrange
            var converter = new CurrencyConverter(_repositoryMock.Object);

            //act
            converter.Convert(1, "USD", "Invalid");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Exception not thrown when currencyTo is invalid")]
        public void Should_Rejected_When_CurrencyTo_Is_Invalid()
        {
            //arrange
            var converter = new CurrencyConverter(_repositoryMock.Object);

            //act
            converter.Convert(1, "Invalid", "USD");
        }
    }
}
