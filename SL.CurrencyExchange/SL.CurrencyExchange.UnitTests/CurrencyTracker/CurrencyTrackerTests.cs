﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SL.CurrencyExchange.Domain.IRepositories;
using SL.CurrencyExchange.Domain.Services;
using SL.CurrencyExchange.UnitTests.Common;

namespace SL.CurrencyExchange.UnitTests.CurrencyTracker
{
    [TestClass]
    public class CurrencyTrackerTests
    {
        private Mock<ICurrencyRepository> _repositoryMock;
        private CurrencyRepositoryMockFactory _currencyRepositoryMockFactory;

        private Mock<IStabilityPercentageCalculator> _stabilityMock;
        private StabilityPercentageCalculatorMockFactory _stabilityMockFactory;

        [TestInitialize]
        public void Initialize()
        {
            _currencyRepositoryMockFactory = new CurrencyRepositoryMockFactory();
            _repositoryMock = _currencyRepositoryMockFactory.CreateInstance();

            _stabilityMockFactory = new StabilityPercentageCalculatorMockFactory();
            _stabilityMock = _stabilityMockFactory.CreateInstance();
        }

        [TestMethod]
        public void Change_History_Should_Returned_Correctly()
        {
            //arrange
            var currencyTracker = new Domain.Services.CurrencyTracker(_repositoryMock.Object, null);

            //act
            var currencyChanges = currencyTracker.GetChangeHistory("USD", "GEL");

            //assert
            Assert.IsTrue(currencyChanges.ContainsKey("USD"), "currencyChanges.ContainsKey('USD')");
            Assert.IsTrue(currencyChanges.ContainsKey("GEL"), "currencyChanges.ContainsKey('GEL')");
            Assert.IsTrue(!currencyChanges.ContainsKey("PLN"), "!currencyChanges.ContainsKey('PLN')");
        }

        [TestMethod]
        public void GetMostStableCurrencies_Should_Work_Correctly()
        {
            //arrange
            var currencyTracker = new Domain.Services.CurrencyTracker(_repositoryMock.Object, _stabilityMock.Object);

            //act
            var stableCurrencies = currencyTracker.GetMostStableCurrencies(2);

            //assert
            Assert.IsTrue(string.Equals(stableCurrencies[0].Key, "USD", StringComparison.InvariantCultureIgnoreCase));
            Assert.IsTrue(string.Equals(stableCurrencies[1].Key, "PLN", StringComparison.InvariantCultureIgnoreCase));
        }

        [TestMethod]
        public void GetMostUnstableCurrencies_Should_Work_Correctly()
        {
            //arrange
            var currencyTracker = new Domain.Services.CurrencyTracker(_repositoryMock.Object, _stabilityMock.Object);

            //act
            var unstableCurrencies = currencyTracker.GetMostUnstableCurrencies(2);

            //assert
            Assert.IsTrue(string.Equals(unstableCurrencies[0].Key, "RUB", StringComparison.InvariantCultureIgnoreCase));
            Assert.IsTrue(string.Equals(unstableCurrencies[1].Key, "GEL", StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
