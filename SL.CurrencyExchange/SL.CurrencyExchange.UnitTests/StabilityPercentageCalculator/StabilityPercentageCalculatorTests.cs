﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SL.CurrencyExchange.Domain.BusinessObjects;

namespace SL.CurrencyExchange.UnitTests.StabilityPercentageCalculator
{

    [TestClass]
    public class StabilityPercentageCalculatorTests
    {
        private Dictionary<string, List<CurrencyRate>> _changesHistoryMock;

        [TestInitialize]
        public void Initialize()
        {
            //(b / a - 1) * 100 ==> change %
            _changesHistoryMock = new Dictionary<string, List<CurrencyRate>>
            {
                {"USD", new List<CurrencyRate> {new CurrencyRate(1), new CurrencyRate(2), new CurrencyRate(3)}},//150
                {"GEL", new List<CurrencyRate> {new CurrencyRate(3), new CurrencyRate(5), new CurrencyRate(2)}},//126.6666
                {"RUB", new List<CurrencyRate> {new CurrencyRate(5), new CurrencyRate(3), new CurrencyRate(1)}}//106.6666
            };
        }

        [TestMethod]
        public void Stability_Percentage_Should_Calculated_Correctly_When_Changes_Are_Ascending()
        {
            //arrange
            var calculator = new Domain.Services.StabilityPercentageCalculator();

            //act
            var stabilities = calculator.Calculate(_changesHistoryMock);

            //assert
            var usdStability = stabilities.SingleOrDefault(e => e.Key.Equals("USD"));
            Assert.IsNotNull(usdStability);
            Assert.AreEqual(decimal.Round(usdStability.Value, 4), 150m);
        }


        [TestMethod]
        public void Stability_Percentage_Should_Calculated_Correctly_When_Changes_Are_Descending()
        {
            //arrange
            var calculator = new Domain.Services.StabilityPercentageCalculator();

            //act
            var stabilities = calculator.Calculate(_changesHistoryMock);

            //assert
            var rubStability = stabilities.SingleOrDefault(e => e.Key.Equals("RUB"));
            Assert.IsNotNull(rubStability);
            Assert.AreEqual(decimal.Round(rubStability.Value, 4), 106.6667m);
        }

        [TestMethod]
        public void Stability_Percentage_Should_Calculated_Correctly_When_Changes_Are_Asc_Desc()
        {
            //arrange
            var calculator = new Domain.Services.StabilityPercentageCalculator();

            //act
            var stabilities = calculator.Calculate(_changesHistoryMock);

            //assert
            var gelStability = stabilities.SingleOrDefault(e => e.Key.Equals("GEL"));
            Assert.IsNotNull(gelStability);
            Assert.AreEqual(decimal.Round(gelStability.Value, 4), 126.6667m);
        }
    }
}
