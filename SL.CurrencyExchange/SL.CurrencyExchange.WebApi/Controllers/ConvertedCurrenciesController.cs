﻿using System.Web.Http;
using SL.CurrencyExchange.Domain.Services;
using SL.CurrencyExchange.Ecb.Data.Repositories;

namespace SL.CurrencyExchange.WebApi.Controllers
{
    [RoutePrefix("api/convertedCurrencies")]
    public class ConvertedCurrenciesController : ApiController
    {
        private readonly CurrencyConverter _converter;

        public ConvertedCurrenciesController()
        {
            _converter = new CurrencyConverter(new EcbCurrencyRepository());//poor man DI :)
        }

        public ConvertedCurrenciesController(CurrencyConverter converter)
        {
            _converter = converter;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult Convert([FromUri]string from, [FromUri]string to, [FromUri]decimal amount)
        {
            var converted = _converter.Convert(amount, from, to);

            return Ok(converted);
        }
    }
}