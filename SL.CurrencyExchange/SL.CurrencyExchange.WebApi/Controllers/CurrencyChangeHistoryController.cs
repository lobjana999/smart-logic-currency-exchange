﻿using System.Linq;
using System.Net;
using System.Web.Http;
using SL.CurrencyExchange.Domain.Services;
using SL.CurrencyExchange.Ecb.Data.Repositories;

namespace SL.CurrencyExchange.WebApi.Controllers
{
    [RoutePrefix("api/currencyChangeHistory")]
    public class CurrencyChangeHistoryController : ApiController
    {
        private readonly CurrencyTracker _currencyTracker;

        public CurrencyChangeHistoryController()
        {
            _currencyTracker = new CurrencyTracker(new EcbCurrencyRepository(), new StabilityPercentageCalculator());//Poor Man DI :)
        }

        public CurrencyChangeHistoryController(CurrencyTracker currencyTracker)
        {
            _currencyTracker = currencyTracker;
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult GetChangeHistory([FromUri]string[] currencies)
        {
            var changeHistory = _currencyTracker.GetChangeHistory(currencies);
            if (changeHistory == null || !changeHistory.Any())
            {
                return StatusCode(HttpStatusCode.NoContent);
            }

            return Ok(changeHistory);
        }
    }
}