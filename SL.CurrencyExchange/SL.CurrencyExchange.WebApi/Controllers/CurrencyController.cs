﻿using System.Linq;
using System.Net;
using System.Web.Http;
using SL.CurrencyExchange.Domain.Services;
using SL.CurrencyExchange.Ecb.Data.Repositories;

namespace SL.CurrencyExchange.WebApi.Controllers
{
    [RoutePrefix("api/currencies")]
    public class CurrencyController : ApiController
    {
        private readonly CurrencyService _currencyService;
        private readonly CurrencyTracker _currencyTracker;

        public CurrencyController()
        {
            //poor man DI :)
            var repository = new EcbCurrencyRepository();
            _currencyTracker = new CurrencyTracker(repository, new StabilityPercentageCalculator());
            _currencyService = new CurrencyService(repository);
        }

        public CurrencyController(CurrencyService currencyService, CurrencyTracker currencyTracker)
        {
            _currencyService = currencyService;
            _currencyTracker = currencyTracker;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult GetAll()
        {
            var currencies = _currencyService.GetAll();
            if (currencies == null || !currencies.Any())
            {
                return StatusCode(HttpStatusCode.NoContent);
            }

            return Ok(currencies);
        }

        [HttpGet]
        [Route("stable")]
        public IHttpActionResult GetMostStable([FromUri] int count)
        {
            var currencies = _currencyTracker.GetMostStableCurrencies(count);
            if (currencies == null || !currencies.Any())
            {
                return StatusCode(HttpStatusCode.NoContent);
            }

            return Ok(currencies);
        }

        [HttpGet]
        [Route("unstable")]
        public IHttpActionResult GetMostUnstable([FromUri] int count)
        {
            var currencies = _currencyTracker.GetMostUnstableCurrencies(count);
            if (currencies == null || !currencies.Any())
            {
                return StatusCode(HttpStatusCode.NoContent);
            }

            return Ok(currencies);
        }
    }
}
